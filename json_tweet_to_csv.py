# Dieser Code wandelt die JSON-Dateien aus dem Twibot-20 Datenset in CSV-Dateien um, so dass diese in den Jupyter Notebooks verwendet werden könne.
# Code in Zusammenarbeit mit ChatGPT entstanden
import json
import csv
def json_tweet_to_csv(twibot_json, twibot_tweet_csv, look_for_bot):
    """
    Saves the user properties as a CSV files based on the defined input JSON.

    Parameters
    ----------
    twibot_json : JSON
        The JSON file of the original dataset.

    twibot_tweet_csv : file path where the CSV shall be saved.
        The CSV file path of the tweet subset.

    look_for_bot : binary
        Iff 1, this function looks for bots. Iff 0, this function looks for genuine users.

    Returns
    -------
    None
        Just saves the CSV files.
    """

    # JSON-Datei einlesen
    with open(twibot_json, 'r', encoding='utf-8') as json_file:
        json_data = json.load(json_file)


    # CSV-Datei öffnen oder erstellen
    with open(twibot_tweet_csv, 'w', newline='', encoding='utf-8') as csv_file:
        csv_writer = csv.writer(csv_file, quoting=csv.QUOTE_ALL)


        # CSV-Header schreiben
        csv_header = ["id", "text", "source", "user_id", "truncated", "in_reply_to_status_id",
                      "in_reply_to_user_id", "in_reply_to_screen_name", "retweeted_status_id", "geo",
                      "place", "contributors", "retweet_count", "reply_count", "favorite_count",
                      "favorited", "retweeted", "possibly_sensitive", "num_hashtags", "num_urls",
                      "num_mentions", "created_at", "timestamp", "crawled_at", "updated"]
        csv_writer.writerow(csv_header)

        debug_zaehler = 0
        # Für jeden Benutzer in der JSON-Datei
        for user_data in json_data:
            # Extrahiere relevante Informationen aus dem JSON-Datenobjekt
            tweet_id = ""
            tweet = user_data.get("tweet", [])
            # text = "\n".join(tweet) if isinstance(tweet, list) else ""
            source = user_data.get("source", "")
            user_id = user_data.get("profile", {}).get("id", "")
            truncated = ""
            in_reply_to_status_id = "0"
            in_reply_to_user_id = ""
            in_reply_to_screen_name = ""
            retweeted_status_id = "0"
            geo = ""
            place = ""
            contributors = ""
            retweet_count = ""
            reply_count = ""
            favorite_count = ""
            favorited = ""
            retweeted = ""
            possibly_sensitive = ""
            num_hashtags = ""
            num_urls = ""
            num_mentions = ""
            created_at = user_data.get("profile", {}).get("created_at", "")
            timestamp = ""
            crawled_at = ""
            updated = ""
            is_bot = user_data.get("label") == "1"

            try:
                for element in tweet:
                    # entferne noch Zeilenumbrüche und Anführungszeichen
                    text_without_newlines_and_quotes = element.replace('\n', '').replace('"', '')
                    text = text_without_newlines_and_quotes
                    # check for retweet
                    retweeted_status_id = "0"
                    in_reply_to_status_id = "0"
                    if text.startswith("RT @"):
                        retweeted_status_id = "1"
                    # check for reply
                    elif text.startswith("@"):
                        in_reply_to_status_id = "1"

                    # Schreibe die Daten in die
                    if is_bot == look_for_bot:
                        csv_writer.writerow([tweet_id, text, source, user_id, truncated, in_reply_to_status_id,
                                             in_reply_to_user_id, in_reply_to_screen_name, retweeted_status_id, geo,
                                             place, contributors, retweet_count, reply_count, favorite_count,
                                             favorited, retweeted, possibly_sensitive, num_hashtags, num_urls,
                                             num_mentions, created_at, timestamp, crawled_at, updated])
            except:
                print("Der Account mit der folgenden ID enthällt keine Tweets:", user_id)


    print("Die Umwandlung von JSON in CSV wurde für eine Datei abgeschlossen.")