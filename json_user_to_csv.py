# Dieser Code wandelt die JSON-Dateien aus dem Twibot-20 Datenset in CSV-Dateien um, so dass diese in den Jupyter Notebooks verwendet werden könne.
# Code in Zusammenarbeit mit ChatGPT entstanden
import json
import csv

def json_account_to_csv(twibot_json, twibot_account_csv, look_for_bot):
    """
    Saves the user properties as a CSV files based on the defined input JSON.

    Parameters
    ----------
    twibot_json : JSON
        The JSON file of the original dataset.

    twibot_account_csv : file path where the CSV shall be saved.
        The CSV file path of the account subset.

    look_for_bot : binary
        Iff 1, this function looks for bots. Iff 0, this function looks for genuine users.

    Returns
    -------
    None
        Just saves the CSV files.
    """
    print("Funktion json_account_to_csv aufgerufen")
    # JSON-Datei einlesen
    with open(twibot_json, 'r', encoding='utf-8') as json_file:
        json_data = json.load(json_file)

    # CSV-Datei öffnen und den Header schreiben

    with open(twibot_account_csv, 'w', newline='', encoding='utf-8') as csv_file:

        csv_writer = csv.writer(csv_file, quoting=csv.QUOTE_ALL)

        header = ["id", "name", "screen_name", "statuses_count", "followers_count", "friends_count", "favourites_count",
                  "listed_count", "url", "lang", "time_zone", "location", "default_profile", "default_profile_image",
                  "geo_enabled", "profile_image_url", "profile_banner_url", "profile_use_background_image",
                  "profile_background_image_url_https", "profile_text_color", "profile_image_url_https",
                  "profile_sidebar_border_color", "profile_background_tile", "profile_sidebar_fill_color",
                  "profile_background_image_url", "profile_background_color", "profile_link_color", "utc_offset",
                  "is_translator", "follow_request_sent", "protected", "verified", "notifications", "description",
                  "contributors_enabled", "following", "created_at", "timestamp", "crawled_at", "updated"]

        csv_writer.writerow(header)


        for entry in json_data:
            # Prüft, ob der Eintrag einen Bot beschreibt. Je nachdem, wonach gesucht wird (oben deklariert), wird in Datei geschrieben.
            is_bot = entry.get("label") == "1"
            if is_bot == look_for_bot:
                csv_writer.writerow([
                    entry.get("ID", ""),
                    entry["profile"].get("name", ""),
                    entry["profile"].get("screen_name", ""),
                    entry["profile"].get("statuses_count", ""),
                    entry["profile"].get("followers_count", ""),
                    entry["profile"].get("friends_count", ""),
                    entry["profile"].get("favourites_count", ""),
                    entry["profile"].get("listed_count", ""),
                    entry["profile"].get("url", ""),
                    entry["profile"].get("lang", ""),
                    entry["profile"].get("time_zone", ""),
                    entry["profile"].get("location", ""),
                    entry["profile"].get("default_profile", ""),
                    entry["profile"].get("default_profile_image", ""),
                    entry["profile"].get("geo_enabled", ""),
                    entry["profile"].get("profile_image_url", ""),
                    entry["profile"].get("profile_banner_url", ""),
                    entry["profile"].get("profile_use_background_image", ""),
                    entry["profile"].get("profile_background_image_url_https", ""),
                    entry["profile"].get("profile_text_color", ""),
                    entry["profile"].get("profile_image_url_https", ""),
                    entry["profile"].get("profile_sidebar_border_color", ""),
                    entry["profile"].get("profile_background_tile", ""),
                    entry["profile"].get("profile_sidebar_fill_color", ""),
                    entry["profile"].get("profile_background_image_url", ""),
                    entry["profile"].get("profile_background_color", ""),
                    entry["profile"].get("profile_link_color", ""),
                    entry["profile"].get("utc_offset", ""),
                    entry["profile"].get("is_translator", ""),
                    entry["profile"].get("follow_request_sent", ""),
                    entry["profile"].get("protected", ""),
                    entry["profile"].get("verified", ""),
                    entry["profile"].get("notifications", ""),
                    entry["profile"].get("description", ""),
                    entry["profile"].get("contributors_enabled", ""),
                    ",".join(entry.get("neighbor", {}).get("following", [])) if entry.get("neighbor") else "",
                    entry["profile"].get("created_at", ""),
                    entry.get("timestamp", ""),
                    entry.get("crawled_at", ""),
                    entry.get("updated", ""),
                ])

    print("Daten wurden erfolgreich in die CSV-Datei übertragen.")