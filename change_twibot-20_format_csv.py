import pandas as pd
import json
import csv
from json_tweet_to_csv import *
from json_user_to_csv import *

def main():
    print("Change format from JSON to CSV.")

    # Load the original dataset
    twibot_test_json = './Twibot-20/test.json'
    twibot_dev_json = './Twibot-20/dev.json'
    twibot_train_json = './Twibot-20/train.json'

    # where to save the files
    twibot_testset_accountdata_botuser_csv = './Twibot-20/twibot_testset_accountdata_botuser.csv'
    twibot_testset_accountdata_genuineuser_csv = './Twibot-20/twibot_testset_accountdata_genuineuser.csv'
    twibot_testset_tweetdata_botuser_csv = './Twibot-20/twibot_testset_tweetdata_botuser.csv'
    twibot_testset_tweetdata_genuineuser_csv = './Twibot-20/twibot_testset_tweetdata_genuineuser.csv'
    twibot_trainset_accountdata_botuser_csv = './Twibot-20/twibot_trainset_accountdata_botuser.csv'
    twibot_trainset_accountdata_genuineuser_csv = './Twibot-20/twibot_trainset_accountdata_genuineuser.csv'
    twibot_trainset_tweetdata_botuser_csv = './Twibot-20/twibot_trainset_tweetdata_botuser.csv'
    twibot_trainset_tweetdata_genuineuser_csv = './Twibot-20/twibot_trainset_tweetdata_genuineuser.csv'

    # Run the script to change the format from JSON to CSV for the tweets in each constallation (1=Bot, 0=Genuine)
    print("Change format for tweets.")
    json_tweet_to_csv(twibot_test_json, twibot_testset_tweetdata_botuser_csv, 1)
    json_tweet_to_csv(twibot_test_json, twibot_testset_tweetdata_genuineuser_csv, 0)
    json_tweet_to_csv(twibot_train_json, twibot_trainset_tweetdata_botuser_csv, 1)
    json_tweet_to_csv(twibot_train_json, twibot_trainset_tweetdata_genuineuser_csv, 0)

    print("change format for accounts.")
    # Run the script to change the format from JSON to CSV for the accounts in each constallation (1=Bot, 0=Genuine)
    json_account_to_csv(twibot_test_json, twibot_testset_accountdata_botuser_csv, 1)
    json_account_to_csv(twibot_test_json, twibot_testset_accountdata_genuineuser_csv, 0)
    json_account_to_csv(twibot_train_json, twibot_trainset_accountdata_botuser_csv, 1)
    json_account_to_csv(twibot_train_json, twibot_trainset_accountdata_genuineuser_csv, 0)#

if __name__ == "__main__":
    main()