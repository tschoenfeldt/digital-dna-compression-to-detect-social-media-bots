# Digital DNA Compression to Detect Social Media Bots – Documented Code
Here, Digital DNA is a way to encode social media activity. It is compressed as an entropy measure to detect social media bots. This repository documents the code I use in my Bachelor's thesis to replicate [Pasricha's & Hayes' (2019)](https://ceur-ws.org/Vol-2563/#aics_35) Digital DNA compression approach. After generating the Digital DNA string, it is compressed and used for supervised learning to tell bots and genuine users apart. Standard metrics are used for evaluation purposes. Firstly, I exactly replicate their approach using the [original code](https://github.com/pasricha/bot-dna-compression) and the [MIB dataset](https://botometer.osome.iu.edu/bot-repository/datasets.html), which includes genuine Twitter users as well as bots. Secondly, I conceptually replicate their approach on the [Twibot-20](https://github.com/BunsenFeng/TwiBot-20) dataset in four test cases. (A more detailed description of what Digital DNA does and what I replicate how can be found in my thesis. Read the section "Research Motivation and Procedure" in Chapter 1 to get a first idea.)
My findings are that the results of [Pasricha & Hayes (2019)](https://ceur-ws.org/Vol-2563/#aics_35) can be fully replicated and confirmed. Applying their approach to the newer [Twibot-20](https://github.com/BunsenFeng/TwiBot-20) dataset does not provide satisfactory results. This suggests the Digital DNA compression approach tested is not a suitable detection method for advanced bots.

## Usage: What is the purpose of the different Jupyter Notebooks?
The notebooks and the Python scripts are used for different purposes. The files ending on `_original.ipynb` are an exact copy of [Pasricha's & Hayes' code](https://github.com/pasricha/bot-dna-compression), including their original results for comparison purposes. The files ending on `_replication.ipynb` are marginally modified versions of the original files. Only minor errors were corrected (mostly UTF-8-continuation byte issues, see Chapter 4, section "Method and Scope of Replication"). The files including `_Twibot-20` in their name are used for the conceptual replication. The Python scripts ending in `.py` were used to change the format of the Twibot-20 dataset from JSON to CSV; this script was coded with the assistance of the GPT-3.5 language model developed by OpenAI. Due to legal reasons, the datasets must not be included in this repository; the [Installation](## Installation) section explains how to download and integrate them.

The MIB dataset in the `datasets_full` folder (name not changed to maintain original structure) includes different subsets. Each of the subsets is stored in a subfolder and includes a `users.csv`, which stores the account information, and a `tweets.csv`, which stores the corresponding tweets with metadata. For a short description of the subsets see "TABLE 2.1: Statistics about the MIB-1 dataset" in my thesis.

The `Twibot-20` folder includes the dataset in name. The `train.json` and `test.json` (training and testing subset) are used to generate the CSV files with the Python scripts.

For a brief presentation of the datasets, see the section "The Benchmark Datasets MIB and Twibot-20" in my thesis. Or read the original descriptions of the dataset.

| File Name                                           | Brief Description                                                                                                                                                              |
|-----------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| dna-compression_original.ipynb                      | Pasricha & Hayes original notebook to calculate compression ratios. Generates Figure 3.1 and the results for Table 3.1. Not modified.                                          |
| dna-compression_replication.ipynb                   | Modified version. Used for exact replication (MIB Dataset). Generates Figure 4.1 and the results for Table 4.1. Changed encoding due to UTF-8 issues, does not impact results. |
| dna-compression_Twibot-20-orginial-method.ipynb     | Modified version. Used for conceptual replication (Twibot-20). Generates Figure 5.1 Corresponds to case 4 in Chapter 5.                                                        |
| dna-compression_twibot-20-original-classifier.ipynb | Modified version. Used for conceptual replication (Twibot-20). Corresponds to cases 1 and 2 in Chapter 5.                                                                      |
| dna-compression_Twibot-20-trained.ipynb             | Modified version. Used for conceptual replication (Twibot-20). Corresponds to case 3 in Chapter 5.                                                                             |
| dna-length_original.ipynb                           | Pasricha & Hayes original notebook to calculate the impact of the Digital DNA length. Generates Figure 3.2. Not modified.                                                      |
| dna-length_replication.ipynb                        | Modfied version. Used for exact replication (MIB Dataset). Changed encoding due to UTF-8 issues, does not impact results.                                                      |
| dna-permutation_original.ipynb                      | Part of Pasricha & Hayes original repository, not used here.                                                                                                                   |
| change_twibot-20_format_csv.py                      | Python script used to change the format of Twibot-20 from JSON to CSV.                                                                                                         |
| conda_environment.yml                               | File for Conda to load the environments (cf. Installation).                                                                                                                    |
| dna-len-1.pdf                                       | Comparison "original DNA size" and "compression ratio" mixed-1. Figure 3.2 A of the thesis.                                                                                    |
| dna-len-1_replication.pdf                           | Comparison "original DNA size" and "compression ratio" mixed-1. Not used in the thesis as it is identical to Figure 3.2 A.                                                     |
| dna-len-2.pdf                                       | Comparison "original DNA size" and "compression ratio" mixed-2. Figure 3.2 B of the thesis.                                                                                    |
| dna-len-2_replication.pdf                           | Comparison "original DNA size" and "compression ratio" mixed-2. Not used in the thesis as it is identical to Figure 3.2 B.                                                     |
| dna-scatter-1.pdf                                   | Plots "Original DNA Size" and "Compressed DNA Size" for mixed 1 and 2 (MIB Datatset). Not included in the thesis.                                                              |
| dna-scatter-1_replication.pdf                       | Plots "Original DNA Size" and "Compressed DNA Size" for mixed 1 and 2 (MIB Datatset). From exact replication. Not included in the thesis.                                      |
| dna-scatter-1_Twibot-20-orginial-method.pdf         | Plots "Original DNA Size" and "Compressed DNA Size" for Twibot-20 dataset. Not included in the thesis.                                                                         |
| dna-scatter-2.pdf                                   | Plots "Original DNA Size" and "Compression Ratio" for mixed 1 and 2 (MIB Datatset). Figure 3.1 of the thesis.                                                                  |
| dna-scatter-2_replication.pdf                       | Plots "Original DNA Size" and "Compressed DNA Size" for mixed 1 and 2 (MIB Datatset). From exact replication. Figure 4.1 of the thesis.                                        |
| dna-scatter-2_Twibot-20-orginial-method.pdf         | Plots "Original DNA Size" and "Compression Ratio" for Twibot-20 dataset. Figure 5.1 of the thesis.                                                                             |
| json_tweet_to_csv.py                                | Used within "change_twibot-20_format_csv.py" to convert tweets stored in the JSON file from Twibot-20 to CSV.                                                                  |
| json_user_to_csv.py                                 | Used within "change_twibot-20_format_csv.py" to convert the account information stored in the JSON file from Twibot-20 to CSV.                                                 |
| README.md                                           | This file.                                                                                                                                                                     |

## Installation
1. Clone this repository.
2. Download the datasets:
   - Download the MIB Dataset from the [Bot Repository](https://botometer.osome.iu.edu/bot-repository/datasets/cresci-2017/cresci-2017.csv.zip) to the folder `datasets_full` and unzip the folder and all subfolders. Make sure everything is saved to the correct folder. If unpacked correctly, e.g. the `crowdflower_results_aggregated.csv`file should be stored in the folder `\code\datasets_full\datasets_full.csv\crowdflower_results.csv`.
   - [Request](shangbin@cs.washington.edu) the [Twibot-20 Dataset](https://github.com/BunsenFeng/TwiBot-20) for research purposes. Make sure to save the JSON files to the `Twibot-20`folder. 
3. Create the Conda environment.
   - Make sure Conda is installed. If not, you can download it [here](https://www.anaconda.com/download/). Create the Conda environment using the provided YML file:
     `conda env create -f conda_environment.yml`
4. Activate the Conda environment:
     `conda activate digital_dna_compression`.
5. Run the script to change the format of the Twibot-20 files from JSON to CSV:
      `python change_twibot-20_format_csv.py`
6. Start Jupyter Notebook:
    `jupyter notebook`

Please contact me if you encounter any difficulties.

The scripts were executed and tested on Windows 10 Home (x64).
## Authors and Acknowledgement
The original code was obtained from [Pasricha's and Hayes' repository](https://github.com/pasricha/bot-dna-compression). I modified it accordingly for the `_replication.ipynb`and `Twibot-20`notebooks. All modifications can be traced via a comparison with the original file and are also briefly mentioned in the comments of the corresponding file. The Python scripts to convert the format of the Twibot-20 datatset were coded with the assistance of the GPT-3.5 language model developed by OpenAI.

### Description of MIB Dataset
Cresci, S., Di Pietro, R., Petrocchi, M., Spognardi, A., & Tesconi, M. (2017). The Paradigm-Shift of Social Spambots: Evidence, Theories, and Tools for the Arms Race. _Proceedings of the 26th International Conference on World Wide Web Companion_, 963–972. <https://doi.org/10.1145/3041021.3055135>

### Description of Twibot-20 Dataset
Feng, S., Wan, H., Wang, N., Li, J., & Luo, M. (2021). TwiBot-20: A Comprehensive Twitter Bot Detection Benchmark. _Proceedings of the 30th ACM International Conference on Information & Knowledge Management_, 4485–4494. <https://doi.org/10.1145/3459637.3482019>

### Description of Pasricha's and Hayes' approach
Pasricha, N., & Hayes, C. (2019). Detecting Bot Behaviour in Social Media using Digital DNA Compression. In E. Curry, M. Keane, A. Ojo, & D. Salwala (Eds.), _Proceedings for the 27th AIAI Irish Conference on Artificial Intelligence and Cognitive Science_ (Vol. 2563, pp. 376–387). CEUR. <https://ceur-ws.org/Vol-2563/#aics_35>